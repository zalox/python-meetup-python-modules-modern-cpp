# Python meetup
## Code and slides from my talk at 25.04.19 at Trondheim Python meetup


## Get the source
```
git clone https://gitlab.com/zalox/python-meetup-python-modules-modern-cpp --recurse-submodules
```

## Build examples
```
# pybind examples
mkdir build
cd build
cmake ..
make
```


```
# c-api examples
cd c-api
python test.py
```


## Test examples
```
cd pybind/class_example
python test.py

cd pybind/heavy_example
time python test.py --python/--cpp

cd pybind/sum_example
python test.py

# embedded example
./build/pybind/embedded_example/embedded_example

```

