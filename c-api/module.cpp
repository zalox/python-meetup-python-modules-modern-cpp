#include <Python.h>

int sum_ints(int a, int b) {
  return a + b;
}

static PyObject *bind_sum(PyObject *, PyObject *args) {
  int a, b;

  if (!PyArg_ParseTuple(args, "ii", &a, &b))
    return NULL;
  
  return PyLong_FromLong(sum_ints(a, b));
}

static PyObject *SumError;

static PyMethodDef math_methods[] = {
  { "sum", bind_sum, METH_VARARGS, "Sum two ints" }
};

static struct PyModuleDef math_module = {
    PyModuleDef_HEAD_INIT,
    "Maths",
    nullptr,
    -1,
    math_methods,
    nullptr,
    nullptr,
    nullptr,
    nullptr
};


PyMODINIT_FUNC PyInit_Maths(void) {
  PyObject *m = nullptr;
  m = PyModule_Create2(&math_module, 1013);
  if (!m) {
    return nullptr;
  }
  SumError = PyErr_NewException("sum.error", nullptr, nullptr);
  Py_IncRef(SumError);
  PyModule_AddObject(m, "error", SumError);
  return m;  
}
