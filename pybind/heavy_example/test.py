from sys import path
path.append("./../../build")

from Prime import next_prime

import argparse

parser = argparse.ArgumentParser()

parser.add_argument('--python', action='store_true')
parser.add_argument('--cpp', action='store_true')
parser.add_argument('--opt_prime', type=int)

args = parser.parse_args()

if not args.cpp and not args.python :
    print("Please add option --python or --cpp")
    exit()

def is_prime(x):
  if x < 2:
    return False
  
  i = 2
  
  while i < x:
    if x % i == 0:
      return False
    i += 1
  
  return True


def py_next_prime(x):
  while not is_prime(x):
    x += 1
    
  return x

def print_prime(prime):
  print("Next prime is " + str(prime))

if not args.opt_prime:
  prime = 1000000000
else:
  prime = args.opt_prime

if args.python:
  print_prime(py_next_prime(prime))

if args.cpp:
  print_prime(next_prime(prime))

