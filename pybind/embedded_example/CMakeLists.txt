add_definitions(-DPYTHON_PATH="${PROJECT_SOURCE_DIR}/pybind/embedded_example")
find_package(Threads)
add_executable(embedded_example main.cpp)
target_link_libraries(embedded_example PRIVATE pybind11::embed ${CMAKE_THREAD_LIBS_INIT})
